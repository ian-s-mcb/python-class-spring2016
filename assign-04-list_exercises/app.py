from __future__ import print_function, division

import random
from functools import reduce


def nested_sum(t):
    """Computes the total of all numbers in a list of lists.
   
    t: list of list of numbers

    returns: number
    """
    return sum([sum(l) for l in t])


def cumsum(t):
    """Computes the cumulative sum of the numbers in t.

    t: list of numbers

    returns: list of numbers
    """
    return [sum(t[:i]) for i in range(1, len(t)+1)]
    

def middle(t):
    """Returns all but the first and last elements of t.

    t: list

    returns: new list
    """
    f, *m, l = t
    return m


def chop(t):
    """Removes the first and last elements of t.

    t: list

    returns: None
    """
    t.pop(0)
    t.pop()


def is_sorted(t):
    """Checks whether a list is sorted.

    t: list

    returns: boolean
    """
    return all([e1 <= e2
        for i1, e1 in enumerate(t)
        for i2, e2 in enumerate(t)
        if i1 < i2])


def is_anagram(word1, word2):
    """Checks whether two words are anagrams

    word1: string or list
    word2: string or list

    returns: boolean
    """
    word2_copy = list(word2)
    l = [(e not in word2_copy) or word2_copy.remove(e) for e in word1]
    return len(word2_copy) == 0 and not any(l)


def has_duplicates(s):
    """Returns True if any element appears more than once in a sequence.

    s: string or list

    returns: bool
    """
    seen = set()
    return any([(e in seen) or seen.add(e) for e in s])


def main():
    t = [[1, 2], [3], [4, 5, 6]]
    print(nested_sum(t))

    t = [1, 2, 3]
    print(cumsum(t))

    t = [1, 2, 3, 4]
    print(middle(t))
    chop(t)
    print(t)

    print(is_sorted([1, 2, 2]))
    print(is_sorted(['b', 'a']))

    print(is_anagram('stop', 'pots'))
    print(is_anagram('different', 'letters'))
    print(is_anagram([1, 2, 2], [2, 1, 2]))

    print(has_duplicates('cba'))
    print(has_duplicates('abba'))


if __name__ == '__main__':
    main()

