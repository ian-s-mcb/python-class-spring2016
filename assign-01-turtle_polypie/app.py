import tkinter as tk
import turtle
import math
import re

# class for Tk application
class App(tk.Frame):

    """ Constructor

    Sets the application's window settings (title, minimum size,
    resizability) and fills application with widgets.
    """
    def __init__(self, master=None):
        tk.Frame.__init__(self, master)

        # creates wigdet variables
        self.side = tk.IntVar(master=self, value=6)
        self.radius = tk.IntVar(master=self, value=75)
        self.color = tk.StringVar(master=self, value='#FF0000')

        # creates booleans for situation checks
        self.firstRun = True
        self.drawingInProcess = False

        self.grid()
        self.createWidgets()


    def createWidgets(self):
        """ Creates the widgets that make up the UI application.
        
        Widgets includes two scales (ie. sliders), one entry (ie. a text
        field), three labels (for the sliders and entry), and a button.
        Widgets are arranged using a grid layout.
        """
        # creates label for sideScale
        sideLabel = tk.Label(self, text='Sides:')
        sideLabel.grid(row=0, column=0)

        # creates sideScale
        # scale's value is connected to a variable that gets used later on
        sideScale = tk.Scale(
            self,
            orient=tk.HORIZONTAL,
            from_=6,
            to=15,
            variable=self.side)
        sideScale.grid(row=0, column=1)

        # creates label for radiusScale
        radiusLabel = tk.Label(self, text='Radius:')
        radiusLabel.grid(row=1, column=0)

        # creates radiusScale
        # scale's value is connected to a variable that gets used later on
        radiusScale = tk.Scale(
            self,
            orient=tk.HORIZONTAL,
            from_=50,
            to=200,
            variable=self.radius)
        radiusScale.grid(row=1, column=1)

        # creates label for colorEntry
        colorLabel = tk.Label(self, text='Color Code:')
        colorLabel.grid(row=2, column=0)

        # creates colorEntry
        colorEntry = tk.Entry(
            self,
            width=7,
            textvariable=self.color)
        colorEntry.grid(row=2, column=1)

        # creates drawButton
        drawButton = tk.Button(self, text='Draw', command=self.draw)
        drawButton.grid(row=3, column=0, columnspan=2)

        # creates turtle
        turtleFrame = tk.Frame(self)
        turtleFrame.grid(row=0, column=2, rowspan=12)
        turtleCanvas = tk.Canvas(turtleFrame)
        self.turtle = turtle.RawTurtle(turtleCanvas)
        turtleCanvas.grid()


    def polypie(self, n, r):
        """Draws a pie divided into radial segments.

        n: number of segments
        r: length of the radial spokes
        """
        angle = 360.0 / n
        for i in range(n):
            self.isosceles(r, angle/2)
            self.turtle.lt(angle)


    def isosceles(self, r, angle):
        """Draws an icosceles triangle.

        The turtle starts and ends at the peak, facing the middle of the
        base.

        r: length of the equal legs
        angle: peak angle in degrees
        """
        y = r * math.sin(angle * math.pi / 180)

        self.turtle.rt(angle)
        self.turtle.fd(r)
        self.turtle.lt(90+angle)
        self.turtle.fd(2*y)
        self.turtle.lt(90+angle)
        self.turtle.fd(r)
        self.turtle.lt(180-angle)


    def draw(self):
        """Draws a turtle pie according to the user's choices.

        The user's choices (radius, number of radial segments, color) are
        obtained from the slider amounts.
        """
        if (self.drawingInProcess):
            print("WARNING! Only draw process is allowed at a time.")
            return

        # prevent simulanteous drawing processes
        self.drawingInProcess = True
    
        # get draw options from sliders
        s = self.side.get()
        r = self.radius.get()
        c = self.color.get()

        # validate color
        if (not re.match('^#([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$', c)):
            print('WARNING! Invalid color value: ' + c)
            c = '#000000'
            self.color.set(c)

        # cleans turtle output (if necessary)
        if (not self.firstRun):
            self.turtle.clear()

        # set turtle color
        self.turtle.color(c)

        # do pie drawing
        print('Drawing pie (sides=' + str(s) + ', radius=' + str(r) + ', color=' + c + ')')
        self.polypie(s, r)
        print("Finished drawing pie")

        # remember that a drawing has been completed
        self.firstRun = False

        # allow the next drawing process to occur
        self.drawingInProcess = False

# start UI loop
if __name__ == '__main__':
    app = App()

    # set window config
    master = app.master
    master.title('Turtle Polypie')
    master.resizable(width=False, height=False)

    app.mainloop()
