import tkinter as tk
import tkinter.ttk as ttk

import controls_c
import turtlechart


class MainApplication(ttk.Frame):
    """Main widget for application

    A Frame that contains the Controls and TurtleChart widgets.
    """
    def __init__(self, parent):
        """Main constructor
        Calls parent constructor, creates widgets (Controls and TurtleChart),
        and sets widget layouts.
        """
        ttk.Frame.__init__(self, parent, padding=5)

        # widgets
        self.controls = controls_c.Controls(self)
        self.turtlechart = turtlechart.TurtleChart(self)

        # layouts
        self.grid_columnconfigure(0, weight=1)
        self.grid_rowconfigure(1, weight=1)
        self.grid(sticky='nsew')
        self.controls.grid(row=0, column=0)
        self.turtlechart.grid(row=1, column=0)


def main():
    root = tk.Tk()
    root.grid_columnconfigure(0, weight=1)
    root.grid_rowconfigure(0, weight=1)
    root.title('Pie Chart')
    app = MainApplication(root)
    app.mainloop()


if __name__ == "__main__":
    main()
