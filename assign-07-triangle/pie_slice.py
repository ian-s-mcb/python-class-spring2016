import math

import isosceles

class PieSlice(isosceles.Isosceles):
    """PieSlice

    A shape that resembles an isosceles triangle with one side that is rounded.
    The shape is defined by a side length and an angle.
    """
    def __init__(self, turtle, a = None, angle = None, fill_color = '#ff00ff',
        pie_center_pos = None):
        """Main constructor

        Calls parent contructor and stores parameters.
        """
        super().__init__(turtle, a, angle)
        self.fill_color = fill_color
        self.pie_center_pos = \
            pie_center_pos if (pie_center_pos != None) else (0, a)


    def a_angle(self):
        """Gets a_angle

        Disabled because PieSlices don't have an angle a.
        """
        raise Exception('Pie slice cannot have angle a')


    def b_angle(self):
        """Gets b_angle

        Disabled because PieSlices don't have an angle a.
        """
        raise Exception('Pie slice cannot have angle b')


    def c_angle(self):
        """Gets c_angle
        """
        return self.c_angle_attr


    def angles(self):
        """Gets all angles
        """
        return str(self.c_angle_attr)


    def draw(self):
        """Draws PieSlice

        Prints string representation of instance, and then draws a filled in
        PieSlice.
        """
        # print string representation
        print(self)

        self.turtle.fillcolor(self.fill_color)
        self.turtle.begin_fill()
        origPos = self.turtle.pos()
        self.turtle.circle(self.a, self.c_angle_attr)
        nextPos = self.turtle.pos()
        self.turtle.goto(self.pie_center_pos)
        self.turtle.end_fill()
        self.turtle.goto(nextPos)


    def __str__(self):
        """String representation
        """
        return "Pie slice with side {} and angle {}".format(
            self.a,
            self.c_angle_attr)
