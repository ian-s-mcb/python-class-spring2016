import math

class Triangle():
    """Triangle

    A Triangle shape defined by three side lengths.
    """
    def __init__(self, turtle, a = None, b = None, c = None):
        """Main constructor

        Computes triangle's angles and stores parameters.
        """
        # stores parameters
        self.turtle = turtle
        self.a = a
        self.b = b
        self.c = c

        # computes angles
        if self.is_triangle():
            self.a_angle_attr = math.acos((self.b ** 2 + self.c ** 2 - self.a ** 2) / (2 * self.b * self.c)) * (360 / (2 * math.pi))
            self.b_angle_attr = math.acos((self.a ** 2 + self.c ** 2 - self.b ** 2) / (2 * self.a * self.c)) * (360 / (2 * math.pi))
            self.c_angle_attr = math.acos((self.a ** 2 + self.b ** 2 - self.c ** 2) / (2 * self.a * self.b)) * (360 / (2 * math.pi))
        else:
            self.a_angle_attr = None
            self.b_angle_attr = None
            self.c_angle_attr = None


    def is_triangle(self):
        """Checks if instance is a valid triangle
        """
        if self.a == None or self.b == None or self.c == None:
            return False
        s = 0.5 * (self.a + self.b + self.c)
        return (s - self.a > 0) and (s - self.b > 0) and (s - self.c > 0)


    def a_angle(self):
        """Gets angle a
        """
        return self.a_angle_attr


    def b_angle(self):
        """Gets angle b
        """
        return self.b_angle_attr


    def c_angle(self):
        """Gets angle c
        """
        return self.c_angle_attr


    def angles(self):
        """Gets all angles
        """
        return 'a: {}, b: {}, c {}'.format(
            self.a_angle_attr,
            self.b_angle_attr,
            self.c_angle_attr)


    def perimeter(self):
        """Computes perimeter of triangle
        """
        if self.is_triangle():
            return self.a + self.b + self.c
        else:
            return None


    def area(self):
        """Computes area of triangle
        """
        if self.is_triangle():
            s = 0.5 * self.perimeter()
            return math.sqrt(s * (s - self.a) * (s - self.b) * (s - self.c))
        else:
            return None


    def draw(self):
        """Draws triangle and prints string representation
        """
        # print string representation
        print(self)

        if self.is_triangle():
            self.turtle.fd(self.a)
            self.turtle.lt(180 - self.c_angle_attr)
            self.turtle.fd(self.b)
            self.turtle.lt(180 - self.a_angle_attr)
            self.turtle.fd(self.c)
            self.turtle.lt(180 - self.b_angle_attr)
        else:
            print('Cannot draw shape')


    def __str__(self):
        """Gets string representation of instance
        """
        return "Triangle with sides (a: {}, b: {}, c: {}), angles ({}), perimeter: {}, area: {}".format(
            self.a,
            self.b,
            self.c,
            self.angles(),
            self.perimeter(),
            self.area())
