import os
import tkinter as tk
from tkinter import filedialog
import tkinter.ttk as ttk

import analyzer


class Controls(ttk.LabelFrame):
    """A LabelFrame containing widgets to control letter analysis

    Gives the user a way to:
        * choose an input text file
        * choose a top letter count
        * start the letter analysis
    """
    def __init__(self, parent):
        """Main constructor

        Calls parent constructor, creates widgets, and sets widget layouts.

        Widgets consist of:
            * three Labels
            * two Buttons
            * one Scale
        """
        ttk.LabelFrame.__init__(self, parent, text='Controls', padding=5)

        # properties
        self.parent = parent
        self.filenameDisplay = tk.StringVar(master=self, value='None')
        self.count = tk.IntVar(master=self, value='5')
        self.inputPath = None

        # widgets
        label1 = ttk.Label(self, text='File: ')
        label2 = ttk.Label(self, textvariable=self.filenameDisplay)
        button1 = ttk.Button(self, text='Choose...', command=self.chooseFile)
        label3 = ttk.Label(self, text='Count: ')
        scale1 = tk.Scale(self, orient=tk.HORIZONTAL, from_=1, to=26, variable=self.count)
        button2 = ttk.Button(self, text='Analyze', command=self.analyzeFile)

        # layouts
        self.grid(sticky='new')
        widgets = [label1, label2, button1, button2, label3]
        for ix, w in enumerate(widgets):
            w.grid(row=0, column=ix)
        scale1.grid(row=0, column=5, sticky='ew')
        self.grid_columnconfigure(5, weight=1)


    def chooseFile(self):
        """Let's the user choose a file to analyze via a file dialog
        """
        inputPath = filedialog.askopenfilename(
            parent=self,
            filetypes=[('Text file', '.txt')])
        if inputPath:
            self.inputPath = inputPath
            name = os.path.basename(inputPath)
            if (len(name) > 15):
                name = name[:15] + '...'
            self.filenameDisplay.set(name)


    def analyzeFile(self):
        """Analyzes a file and displays results

        Analysis results are displayed on the TurtleChart widget.
        """
        if self.inputPath:
            try: 
                freq = analyzer.letterProbability(self.inputPath, self.count.get())
                self.parent.turtlechart.draw(freq)
            except Exception as e:
                print(e)
