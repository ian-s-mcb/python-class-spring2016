import math

import triangle

class Isosceles(triangle.Triangle):
    """An isoscles triangle shape

    The shape defined by one side length and one angle.
    """
    def __init__(self, turtle, a = None, angle = None):
        """Main constructor

        Calls parent constructor with the computed third side length.
        """
        other_side = math.sqrt((2 * (a **2)) - (2 * (a ** 2) * math.cos(angle * ((2 * math.pi) / 360))))
        super().__init__(turtle, a, a, other_side)


    def is_triangle(self):
        """Checks if shape is a valid triangle
        """
        return True


    def __str__(self):
        """Gets string representation of instance
        """
        return "Isoceles with sides (a: {}, b: {}, c: {}), angles ({}), perimeter: {}, area: {}".format(
            self.a,
            self.b,
            self.c,
            self.angles(),
            self.perimeter(),
            self.area())
