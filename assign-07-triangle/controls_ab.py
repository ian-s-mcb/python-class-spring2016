import tkinter as tk
import tkinter.ttk as ttk

import triangle, isosceles, pie_slice


class ControlsAB(ttk.Frame):
    """A tkinter Frame containing widgets to control shape creation
    """
    def __init__(self, parent, turtle):
        """Main constructor

        Calls parent constructor, creates widgets, and sets widget layouts.

        Widgets consist of: LabelFrames, Labels, Entries, and Buttons.
        """
        ttk.Frame.__init__(self, parent, padding=5)

        # properties
        self.parent = parent
        self.turtle = turtle
        self.tri_a = tk.StringVar(value='30')
        self.tri_b = tk.StringVar(value='40')
        self.tri_c = tk.StringVar(value='50')
        self.iso_a = tk.StringVar(value='100')
        self.iso_angle = tk.StringVar(value='90')
        self.pie_a = tk.StringVar(value='50')
        self.pie_angle = tk.StringVar(value='45')

        # triangle widgets
        label_frame1 = ttk.LabelFrame(self, text='Triangle')
        label1 = ttk.Label(label_frame1, text='A: ')
        entry1 = ttk.Entry(label_frame1, width=3, textvariable=self.tri_a)
        label2 = ttk.Label(label_frame1, text='B: ')
        entry2 = ttk.Entry(label_frame1, width=3, textvariable=self.tri_b)
        label3 = ttk.Label(label_frame1, text='C: ')
        entry3 = ttk.Entry(label_frame1, width=3, textvariable=self.tri_c)
        button1 = ttk.Button(label_frame1, text='Draw', command=self.create_triangle)

        # isosceles widgets
        label_frame2 = ttk.LabelFrame(self, text='Isosceles')
        label4 = ttk.Label(label_frame2, text='A: ')
        entry4= ttk.Entry(label_frame2, width=3, textvariable=self.iso_a)
        label5 = ttk.Label(label_frame2, text='Angle: ')
        entry5 = ttk.Entry(label_frame2, width=3, textvariable=self.iso_angle)
        button2 = ttk.Button(label_frame2, text='Draw', command=self.create_isosceles)

        # slice widgets
        label_frame3 = ttk.LabelFrame(self, text='Slice')
        label6 = ttk.Label(label_frame3, text='A: ')
        entry6= ttk.Entry(label_frame3, width=3, textvariable=self.pie_a)
        label7 = ttk.Label(label_frame3, text='Angle: ')
        entry7 = ttk.Entry(label_frame3, width=3, textvariable=self.pie_angle)
        button3 = ttk.Button(label_frame3, text='Draw', command=self.create_pie_slice)
    
        # clear button
        button4 = ttk.Button(self, text='Clear', command=self.clear)

        # layouts
        self.grid(sticky='new')
        self.grid_columnconfigure(0, weight=1)
        label_frame1.grid(row=0, column=0, sticky='ew')
        label_frame2.grid(row=1, column=0, sticky='ew')
        label_frame3.grid(row=2, column=0, sticky='ew')
        row1 = [label1, entry1, label2, entry2, label3, entry3, button1]
        row2 = [label4, entry4, label5, entry5, button2]
        row3 = [label6, entry6, label7, entry7, button3]
        for ix, w in enumerate(row1):
            w.grid(row=0, column=ix)
        for ix, w in enumerate(row2):
            w.grid(row=0, column=ix)
        for ix, w in enumerate(row3):
            w.grid(row=0, column=ix)
        button4.grid(row=3, column=0, sticky='ew')


    def create_triangle(self):
        """Creates a triangle based off user-provided properties
        """
        tri = triangle.Triangle(
            self.turtle,
            float(self.tri_a.get()),
            float(self.tri_b.get()),
            float(self.tri_c.get()))
        tri.draw()


    def create_isosceles(self):
        """Creates a isosceles tiangle based off user-provided properties
        """
        iso = isosceles.Isosceles(
            self.turtle,
            float(self.iso_a.get()),
            float(self.iso_angle.get()))
        iso.draw()


    def create_pie_slice(self):
        """Creates a pie slice based off user-provided properties
        """
        pie = pie_slice.PieSlice(
            self.turtle,
            float(self.pie_a.get()),
            float(self.pie_angle.get()))
        pie.draw()


    def clear(self):
        """Clears turtle canvas
        """
        self.turtle.penup()
        self.turtle.goto(0, 0)
        self.turtle.setheading(0)
        self.turtle.clear()
        self.turtle.pendown()
        self.turtle.fillcolor('#fff')
