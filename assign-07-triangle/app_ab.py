import tkinter as tk
import tkinter.ttk as ttk
import turtle

import controls_ab


class MainApplication(ttk.Frame):
    """Main widget for application

    A Frame that contains the Controls widget and the turtle canvas
    """
    def __init__(self, parent):
        """Main constructor
        Calls parent constructor, creates widgets (Controls and Canvas),
        and sets widget layouts.
        """
        ttk.Frame.__init__(self, parent, padding=5)

        # creates widgets
        self.canvas = tk.Canvas(self, width=400, height=400)
        self.turtle = turtle.RawTurtle(self.canvas)
        self.turtle.speed(10)
        self.controls = controls_ab.ControlsAB(self, self.turtle)

        # layouts
        self.grid_columnconfigure(0, weight=1)
        self.grid_rowconfigure(1, weight=1)
        self.grid(sticky='nsew')
        self.controls.grid(row=0, column=0)
        self.canvas.grid(row=1, column=0, sticky='nsew')


def main():
    root = tk.Tk()
    root.grid_columnconfigure(0, weight=1)
    root.grid_rowconfigure(0, weight=1)
    root.title('App - parts a,b')
    app = MainApplication(root)
    app.mainloop()


if __name__ == "__main__":
    main()
