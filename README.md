## Assignments from the Spring 2016 Python class (CSC-133)

#### Topics covered in class

Basic syntax (variables, expressions, and statements), functions, conditionals and recursion, iterations, strings, lists, dictionaries, files, classes (objects, functions, methods, inheritance).

#### Reports

Assignment 1 - Turtle Polypie - [report][report-1]  
![Assignment 1 Screenshot][screenshot-1]

Assignment 2 - Caesar Cypher - [report][report-2]  
![Assignment 2 Screenshot][screenshot-2]

Assignment 3 - Top Words - [report][report-3]  
![Assignment 3 Screenshot][screenshot-3]

Assignment 4 - List Exercises - [report][report-4]  
![Assignment 4 Screenshot][screenshot-4]

Assignment 5 - Euclidean algorithm - Skipped

Assignment 6 - Turtle Pie Chart - [report][report-6]  
![Assignment 6 Screenshot][screenshot-6]

Assignment 7 - Turtle Triangle Classes - [report][report-6]  
![Assignment 7 Screenshot][screenshot-7]

[report-1]: ../../raw/master/assign-01-turtle_polypie/turtle_polypie-report.pdf
[report-2]: ../../raw/master/assign-02-caesar_cypher/caesar_cypher-report.pdf
[report-3]: ../../raw/master/assign-03-top_words/top_words-report.pdf
[report-4]: ../../raw/master/assign-04-list_exercises/list_exercises-report.pdf
[report-6]: ../../raw/master/assign-06-piechart/turtle_pie_chart-report.pdf
[report-7]: ../../raw/master/assign-07-triangle/turtle_triangle_classes-report.pdf
[screenshot-1]: https://raw.githubusercontent.com/ian-s-mcb/python-class-spring2016/master/assign-01-turtle_polypie/screenshots/turtle_polypie-screenshot-02.png
[screenshot-2]: https://raw.githubusercontent.com/ian-s-mcb/python-class-spring2016/master/assign-02-caesar_cypher/screenshots/caesar_cipher-screenshot-03-inline_mode.png
[screenshot-3]: https://raw.githubusercontent.com/ian-s-mcb/python-class-spring2016/master/assign-03-top_words/screenshots/top_words-screenshot-01-top_of_top_100.png
[screenshot-4]: https://raw.githubusercontent.com/ian-s-mcb/python-class-spring2016/master/assign-04-list_exercises/screenshots/list_exercises-screenshot-01-command_output.png
[screenshot-6]: https://raw.githubusercontent.com/ian-s-mcb/python-class-spring2016/master/assign-06-piechart/screenshots/pie_chart-screenshot-01-top_five_normal_size.png
[screenshot-7]: https://raw.githubusercontent.com/ian-s-mcb/python-class-spring2016/master/assign-07-triangle/screenshots/screenshot-01-triangle-01.png
