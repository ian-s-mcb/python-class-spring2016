from tkinter import *
from tkinter import filedialog
import string
import re
import os


# class for Tk application
class App(Frame):

    """ Constructor

    Creates the caesar cipher app with various properties and widgets.
    """
    def __init__(self, master=None):
        Frame.__init__(self, master, padx=5, pady=5)

        # creates wigdet variables
        self.shift = IntVar(master=self, value=1)
        self.inputFilename = StringVar(master=self, value="None")
        self.outputFilename = StringVar(master=self, value="None")

        # keeps track of filepaths
        self.inputPath = None
        self.outputPath = None

        # keeps track of entry element
        self.entryShift = None

        # creates elements for inline mode
        self.inputInlineMode = False
        self.outputInlineMode = False
        self.inputInlineElement = None
        self.outputInlineElement = None

        self.createWidgets()
        self.grid()


    def createWidgets(self):
        """ Creates the widgets that make up the UI application.

        Widgets are placed with one of three LabelFrames related to their
        purpose: shift, input, and output.

        The shift frame allows for the detecting, applying and
        removing of the cipher shift (also called a key). The input frame lets
        user specify text input, which can be in the form of a file or text
        typed into a Text widget (which I call inline mode). The output frame
        offers the same file/inline options for output.

        """
        # shift operations
        frame1 = LabelFrame(self, text="Shift Operations", padx=5, pady=5)
        label1 = Label(frame1, text="Amount: ")
        label1.grid(row=0, column=0)
        entry1 = self.entryShift = Entry(frame1, textvariable=self.shift, width=2)
        entry1.grid(row=0, column=1)
        button1 = Button(frame1, text="Detect", command=self.detectShift)
        button1.grid(row=0, column=2)
        button2 = Button(frame1, text="Apply", command=lambda: self.shiftOperation(False))
        button2.grid(row=0, column=3)
        button3 = Button(frame1, text="Remove", command=lambda: self.shiftOperation(True))
        button3.grid(row=0, column=4)
        frame1.grid(row=0, column=0, sticky=N+S+E+W)

        # input file
        frame2 = self.frameInput = LabelFrame(self, text="Input", padx=5, pady=5)
        label3 = Label(frame2, text="File: ")
        label3.grid(row=0, column=0)
        label4 = self.labelInput = Label(frame2, textvariable=self.inputFilename)
        label4.grid(row=0, column=1, sticky=W)
        button4 = Button(frame2, text="Choose...", command=self.setInputFile)
        button4.grid(row=0, column=2)
        button5 = self.buttonInlineInput = Button(frame2, text="Inline", command=self.flipInputInlineMode)
        button5.grid(row=0, column=3)
        frame2.grid_columnconfigure(1, weight=1)
        frame2.grid(row=1, column=0, sticky=N+S+E+W)

        # output file
        frame3 = self.frameOutput = LabelFrame(self, text="Output", padx=5, pady=5)
        label5 = Label(frame3, text="File: ")
        label5.grid(row=0, column=0)
        label6 = self.labelOutput = Label(frame3, textvariable=self.outputFilename)
        label6.grid(row=0, column=1, sticky=W)
        button6 = Button(frame3, text="Choose...", command=self.setOutputFile)
        button6.grid(row=0, column=2)
        button7 = self.buttonInlineOutput = Button(frame3, text="Inline", command=self.flipOutputInlineMode)
        button7.grid(row=0, column=3)
        frame3.grid_columnconfigure(1, weight=1)
        frame3.grid(row=2, column=0, sticky=N+S+E+W)


    def setInputFile(self):
        """Switches to file input mode and shows a file dialog

        If the app was previously in inline input mode, the inline widget is
        destroyed. After showing a file dialog, the chosen file's path is saved
        in class property.
        """
        if self.inputInlineMode:
            self.inputInlineElement.destroy()
            self.buttonInlineInput.config(text="Inline")
            self.inputInlineMode = False
        inputPath = self.inputPath = filedialog.askopenfilename(parent=self, filetypes=[("Text file", ".txt")])
        if self.inputPath:
            name = os.path.basename(inputPath)
            if (len(name) > 15):
                name = name[:15] + "..."
            self.inputFilename.set(name)


    def setOutputFile(self):
        """Switches to file output mode and shows a file dialog

        If the app was previously in output inline mode, the inline widget is
        destroyed. After showing a file dialog, the chosen file's path is saved
        in class property.
        """
        if self.outputInlineMode:
            self.outputInlineElement.destroy()
            self.buttonInlineOutput.config(text="Inline")
            self.outputInlineMode = False
        outputPath = self.outputPath = filedialog.asksaveasfilename(parent=self, filetypes=[("Text file", ".txt")])
        if self.outputPath:
            name = os.path.basename(outputPath)
            if (len(name) > 15):
                name = name[:15] + "..."
            self.outputFilename.set(name)


    def detectShift(self):
        """Detects the shift of the input text

        Input text can be contained in a file or the inline element. The
        detected shift amount is displayed in a Entry widget that is flashed
        red for one second.
        """
        if (not self.inputInlineMode and self.inputPath):
            self.shift.set(lowestChiSquareSum(self.inputPath)[0])
            self.flashTextColor(self.entryShift, 1)

        elif (self.inputInlineMode):
            textIn = self.inputInlineElement.get(1.0, END)
            self.shift.set(lowestChiSquareSumInline(textIn)[0])
            self.flashTextColor(self.entryShift, 1)


    def shiftOperation(self, toPlaintext):
        """Applies or removes a shift to input text

        Input text can be contained in a file or the inline element. The
        output text is either written to a file or displayed on the inline
        output widget (which gets flashed in red for one second.)

        toPlaintext -- Boolean, True if removing a shift, False if applying a
                                shift.
        """
        if (    not self.inputInlineMode and
                not self.outputInlineMode and
                self.inputPath and
                self.outputPath
            ):
            applyShift(toPlaintext, self.inputPath, self.outputPath, self.shift.get())
            self.flashTextColor(self.labelOutput, 1)

        elif (self.inputInlineMode and self.outputInlineMode):
            textIn = self.inputInlineElement.get(1.0, END)
            shifted = applyShiftInline(toPlaintext, textIn, self.shift.get())
            self.outputInlineElement.replace(1.0, END, shifted)
            self.flashTextColor(self.outputInlineElement, 1)

        # TODO add operation code for mixed inline/file


    def flashTextColor(self, label, count):
        """Flashes the color of widget's text in red

        Used recursively to change text color to red and then back to black.
        """
        currentColor = label.cget("foreground")
        nextColor = "black" if currentColor == "red" else "red"
        label.config(foreground=nextColor)
        if count > 0:
            self.after(750, lambda: self.flashTextColor(label, count - 1))


    def flipInputInlineMode(self):
        """ Flips the input mode between file and inline

        If currently in file mode, the mode is changed to inline, and vice
        versa. When necessary the input inline element is created/destroyed.
        """
        if (not self.inputInlineMode):
            self.inputInlineElement = Text(self.frameInput, height=10, width=1)
            self.inputInlineElement.grid(row=1, column=0, columnspan=5, sticky=N+S+E+W)
            self.buttonInlineInput.config(text="Collapse")
        else:
            self.inputInlineElement.destroy()
            self.buttonInlineInput.config(text="Inline")
        self.inputInlineMode = not self.inputInlineMode
        self.inputFilename.set("None")
        self.inputPath = None


    def flipOutputInlineMode(self):
        """ Flips the output mode between file and inline

        If currently in file mode, the mode is changed to inline, and vice
        versa. When necessary the output inline element is created/destroyed.
        """
        if (not self.outputInlineMode):
            self.outputInlineElement = Text(self.frameOutput, height=10, width=1)
            self.outputInlineElement.grid(row=1, column=0, columnspan=5, sticky=N+S+E+W)
            self.buttonInlineOutput.config(text="Collapse")
        else:
            self.outputInlineElement.destroy()
            self.buttonInlineOutput.config(text="Inline")
        self.outputInlineMode = not self.outputInlineMode
        self.outputFilename.set("None")
        self.outputPath = None

# Dict of English language letter frequency
# Taken from Wikipedia:
# https://en.wikipedia.org/wiki/Letter_frequency#Relative_frequencies_of_letters_in_the_English_language
expectedLetterFreq = {
    'a': 0.11602,
    'b': 0.04702,
    'c': 0.03511,
    'd': 0.0267,
    'e': 0.02007,
    'f': 0.03779,
    'g': 0.0195,
    'h': 0.07232,
    'i': 0.06286,
    'j': 0.00597,
    'k': 0.0059,
    'l': 0.02705,
    'm': 0.04383,
    'n': 0.02365,
    'o': 0.06264,
    'p': 0.02545,
    'q': 0.00173,
    'r': 0.01653,
    's': 0.07755,
    't': 0.16671,
    'u': 0.01487,
    'v': 0.00649,
    'w': 0.06753,
    'x': 0.00017,
    'y': 0.0162,
    'z': 0.00034
}


def applyShiftInline(toPlaintext, textIn, n):
    """Applies/removes a shift to an input text (inline ver.)

    Leaves non-alphabetic characters in placed, but has the side-effect of
    putting all letters in lowercase.

    toPlaintext -- Boolean, True if removing a shift, False if applying a shift.
    textIn -- String, the text input to manipulated
    n -- Int, the shift amount
    """
    if (toPlaintext):
        n *= -1
    return "".join([chr(((ord(c) - ord("a") + n) % 26) + ord("a")) if c.isalpha() else c for c in textIn.lower()])


def lowestChiSquareSumInline(textIn):
    """Determines the lowest shift with the lowest Chi Squared sum (Inline ver.)

    The Chi Squared test is statistical hypothesis test that is explained on
    Wikipedia[1] and the following Trinity College article[2]. The test is
    performed on the whole input text with every different shift amount. The
    test compares the letter frequency of the shifted text to the
    expectedLetterFreq Dict. The shift with the lowest Chi Squared Sum exhibits
    the most probable letter frequency.

    textIn -- String, The input text to analyzed

    Returns a tuple with the lowest Chi Square Sum and the corresponding shift
    amount.

    [1]: https://en.wikipedia.org/wiki/Chi-squared_test
    [2]: http://www.cs.trincoll.edu/~crypto/historical/caesar.html
    """
    chiSquareSums = []

    # tries each n value
    for n in range(26):
        actualLetterCount = dict.fromkeys(string.ascii_lowercase, 0)
        chiSquares = dict(actualLetterCount)
        totalLetterCount = 0

        # removes non-alphabetic characters
        textNew = re.sub('[^a-z]+', '', textIn.lower())
        # don't shift word if n=0
        if (n != 0):
            textNew = "".join([chr(((ord(c) - ord("a") - n) % 26) + ord("a")) for c in textNew])

        for c in textNew:
            actualLetterCount[c] += 1
            totalLetterCount += 1

        # computes letter std freq
        for c in actualLetterCount:
            expectedLetterCount = expectedLetterFreq[c] * totalLetterCount
            chiSquares[c] = ((actualLetterCount[c] - expectedLetterCount) ** 2) / expectedLetterCount

        # stores sum of chi squares for the current n value
        chiSquareSums.append((n, round(sum(chiSquares.values()), 2)))

    return min(chiSquareSums, key=lambda x: x[1])


def applyShift(toPlaintext, filepathIn, filepathOut, n):
    """Applies/removes a shift to an input text (file ver.)

    Leaves non-alphabetic characters in placed, but has the side-effect of
    putting all letters in lowercase.

    toPlaintext -- Boolean, True if removing a shift, False if applying a shift.
    filepathIn -- String, path to input file
    filepathOut -- String, path to output file
    n -- Int, the shift amount
    """
    if (toPlaintext):
        n *= -1
    fileIn = open(filepathIn, "r")
    fileOut = open(filepathOut, "w")
    for line in fileIn:
        lineShifted = "".join([chr(((ord(c) - ord("a") + n) % 26) + ord("a")) if c.isalpha() else c for c in line.lower()])
        fileOut.write(lineShifted)

    fileIn.close()
    fileOut.close()


def lowestChiSquareSum(filepathIn):
    """Determines the lowest shift with the lowest Chi Squared sum (file ver.)

    The Chi Squared test is statistical hypothesis test that is explained on
    Wikipedia[1] and the following Trinity College article[2]. The test is
    performed on the whole input text with every different shift amount. The
    test compares the letter frequency of the shifted text to the
    expectedLetterFreq Dict. The shift with the lowest Chi Squared Sum exhibits
    the most probable letter frequency.

    filepathIn -- String, path to input file

    Returns a tuple with the lowest Chi Square Sum and the corresponding shift
    amount.

    [1]: https://en.wikipedia.org/wiki/Chi-squared_test
    [2]: http://www.cs.trincoll.edu/~crypto/historical/caesar.html
    """
    fileIn = open(filepathIn, "r")

    chiSquareSums = []

    # tries each n value
    for n in range(26):
        actualLetterCount = dict.fromkeys(string.ascii_lowercase, 0)
        chiSquares = dict(actualLetterCount)
        totalLetterCount = 0

        # applies n to each word and update letter statistics
        for line in fileIn:
            # removes non-alphabetic characters
            line = re.sub('[^a-z]+', '', line.lower())
            # don't shift word if n=0
            if (n != 0):
                line = "".join([chr(((ord(c) - ord("a") - n) % 26) + ord("a")) for c in line])

            for c in line:
                actualLetterCount[c] += 1
                totalLetterCount += 1

        # computes letter std freq
        for c in actualLetterCount:
            expectedLetterCount = expectedLetterFreq[c] * totalLetterCount
            chiSquares[c] = ((actualLetterCount[c] - expectedLetterCount) ** 2) / expectedLetterCount

        # stores sum of chi squares for the current n value
        chiSquareSums.append((n, round(sum(chiSquares.values()), 2)))

        fileIn.seek(0)

    fileIn.close()
    return min(chiSquareSums, key=lambda x: x[1])


# starts UI loop
if __name__ == '__main__':
    app = App()

    # sets window config
    master = app.master
    master.title('Caesar Cypher')
    master.resizable(width=False, height=False)

    app.mainloop()
