import math
import tkinter as tk
import tkinter.ttk as ttk
import turtle


class TurtleChart(ttk.Frame):
    """A Frame for drawing a turtle graphics pie chart

    Accepts pie chart data and draws it on a Tkinter Canvas. The chart contains
    pie slices, pie slice labels, and a legend.
    """
    def __init__(self, parent):
        """Main constructor

        Calls parent constructor, sets class properties, creates widgets
        (Canvas and Turtle), and sets widget layouts.
        """
        ttk.Frame.__init__(self, parent)

        self.setProperties()

        # widgets
        self.canvas = tk.Canvas(self, width=self.w, height=self.h)
        self.turtle = turtle.RawTurtle(self.canvas)
        self.turtle.speed(10)

        # layouts
        self.grid(sticky='nsew')
        self.grid_rowconfigure(0, weight=1)
        self.grid_columnconfigure(0, weight=1)
        self.canvas.grid(row=0, column=0, sticky='nsew')


    def setProperties(self):
        """ Sets class properties
        """
        # width and height for the Tkinter Canvas
        self.w, self.h = 400, 400

        # these 26 colors came from the following stackoverflow post
        # http://stackoverflow.com/questions/2328339/how-to-generate-n-different-colors-for-any-natural-number-n/20298116#20298116
        self.colors = [
            '#FFFF00', '#1CE6FF', '#FF34FF', '#FF4A46', '#008941', '#006FA6',
            '#A30059', '#FFDBE5', '#7A4900', '#0000A6', '#63FFAC', '#B79762',
            '#004D43', '#8FB0FF', '#997D87', '#5A0007', '#809693', '#FEFFE6',
            '#1B4400', '#4FC601', '#3B5DFF', '#4A3B53', '#FF2F80', '#61615A',
            '#BA0900', '#6B7900']
       
        # data components
        self.labels = []
        self.probabilities = []
        self.angles = []
        self.labelAngles = []

        # chart size details
        self.pieStartPos = None
        self.pieRadius = None
        self.pieCenterPos = None
        self.legendStartPos = None
        self.labelStartPos = None
        self.labelDistance = None


    def organizeData(self, data):
        """Organizes data so that it can be easily used

        data -- List, (see analyzer.py for a full description)
        """
        # clears previous data
        self.labels.clear()
        self.probabilities.clear()
        self.angles.clear()
        self.labelAngles.clear()

        for e in data:
            self.labels.append(e[0].capitalize())
            self.probabilities.append(e[1])
            self.angles.append(e[1] * 360)

        # label angles bisect two regular angles
        self.labelAngles.append(self.angles[0] / 2)
        for i in range(1, len(data)):
            self.labelAngles.append((self.angles[i - 1] + self.angles[i]) / 2)


    def detectChartSize(self):
        """Detects what size chart to draw

        The following measurements are chosen with respect to the current size
        of the Tkinter Canvas:
            * pie start position
            * pie radius
            * pie center position
            * legend start position
            * label start position
        """
        # pie start position is equal to:
        #      horizontal middle of window + shift to fit legend
        #      vertical bottom of window + shift to prevent cutoff
        x = self.canvas.winfo_width()
        y = self.canvas.winfo_height()
        shiftX = -40
        shiftY = 25
        self.pieStartPos = (x/2 - self.w/2 + shiftX, -y + self.h/2 + shiftY)

        # chooses a pie radius that fills canvas
        pad = 85
        self.pieRadius = (min(x, y) / 2) - pad

        # pie center position (relative to pie start position)
        self.pieCenterPos = (
            self.pieStartPos[0],
            self.pieStartPos[1] + self.pieRadius)

        # legend start position (north east of pie)
        legendShift = 20
        self.legendStartPos = (
            self.pieStartPos[0] + self.pieRadius + legendShift,
            self.h/2 - legendShift)

        # label start position
        # lower than pie start position because labels are outside the pie
        self.labelDistance = 25
        self.labelStartPos = (
            self.pieStartPos[0],
            self.pieStartPos[1] - self.labelDistance)


    def drawSlices(self):
        """Draws pie chart slices

        Slice angles correspond to the probability data. The slice drawing is
        primarily handled by Turtle's circle method, which draws either arcs or
        circles.
        """
        # puts turtle in start position
        self.turtle.penup()
        self.turtle.goto(self.pieStartPos)
        self.turtle.setheading(0)
        self.turtle.pendown()

        for angle, color in zip(self.angles, self.colors):
            self.turtle.fillcolor(color)
            self.turtle.begin_fill()
            self.turtle.circle(self.pieRadius, angle)
            nextPos = self.turtle.pos()
            self.turtle.goto(self.pieCenterPos)
            self.turtle.end_fill()
            self.turtle.goto(nextPos)


    def drawLabels(self):
        """Draws labels for pie chart slices

        Labels are drawed around the perimeter of the slices.
        """
        labelFont = ('Arial', '15', 'normal')
        self.turtle.penup()
        self.turtle.goto(self.labelStartPos)
        for label, angle in zip(self.labels, self.labelAngles):
            self.turtle.circle(self.pieRadius + self.labelDistance, angle)
            self.turtle.write(label, align='center', font=labelFont)


    def drawLegend(self):
        """Draws legend for the pie chart slices

        Legend contains color swatches, labels, and probabilities for the
        slices. Legend is drawn north east of the pie.
        """
        # prepares legend
        swatchSize = 10
        itemSpacing = swatchSize + 8
        oneTimeGap = itemSpacing - 5 # accounts for font height
        legendFont = ('Arial', '10', 'normal')
        self.turtle.penup()
        self.turtle.goto(self.legendStartPos)

        # draws legend swatches
        for color in self.colors[:len(self.probabilities)]:
            self.turtle.setheading(0)
            self.turtle.pendown()
            self.turtle.fillcolor(color)
            self.turtle.begin_fill()
            for i in range(4):
                self.turtle.fd(swatchSize)
                self.turtle.lt(-90)
            self.turtle.end_fill()
            self.turtle.penup()
            self.turtle.lt(-90)
            self.turtle.fd(itemSpacing)

        # draws legend text
        self.turtle.goto(self.legendStartPos)
        self.turtle.setheading(0)
        self.turtle.fd(itemSpacing)
        self.turtle.lt(-90)
        self.turtle.fd(oneTimeGap)
        for label, p in zip(self.labels, self.probabilities):
            self.turtle.write(
                label + ' ' + str(p),
                align='left',
                font=legendFont)
            self.turtle.fd(itemSpacing)


    def draw(self, data):
        """Draws pie chart

        data -- List, (see analyzer.py for full description)
        """
        self.organizeData(data)
        self.detectChartSize()

        # clears previous draw
        self.turtle.clear()

        self.drawSlices()
        self.drawLabels()
        self.drawLegend()
        
        # move turtle out of the way
        self.turtle.goto(self.pieStartPos)
