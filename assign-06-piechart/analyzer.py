from collections import Counter
import re
import string


def letterProbability(filePath, count):
    """Computes the probabilities of all letters within an input file

    filePath -- String, path to input file
    count -- Int, number of top letters to calculate

    returns -- List

    The output list contains tuples (with each tuple containing two components).
    The first component is the letter, and the second component is the letter's
    probability. The list is sorted in order of descending probabilities, except
    for the last element, which is a special case.

    The last list element is the sum all probabilities of the "unitemized"
    letters. Unitemized letters are the letters that are excluded by the count
    parameter.

    Non-alphabetic characters within the input file are ignored. Additionally,
    all letters are put into lowercase.

    See the following examples for a demo of this function's behavior. The
    examples involve an input file that contains just one word: azea.
    
    Examples:
        >>> letterProbability('./someFile.txt', 1)
        <<< [('a', 0.5), ('others', 0.5)]

        >>> letterProbability('~/someFile.txt', 2)
        <<< [('a', 0.5), ('e', 0.25), ('others', 0.25)]

        >>> letterProbability('~/someFile.txt', 3)
        <<< [('a', 0.5), ('e', 0.25), ('z', 0.25), ('others', 0.0)]
    """
    counter = Counter()
    precision = 3

    fileIn = open(filePath, 'r')
    totalLetters = 0
    for line in fileIn:
        line = re.sub(r'[^a-z]', '', line.lower())
        counter.update(line)
        totalLetters += len(line)
    fileIn.close()

    if not totalLetters:
        raise Exception('Nothing to analyze. Input file is empty')

    probAll = [
        (letter, round(counter.get(letter, 0) / totalLetters, precision))
        for letter in string.ascii_lowercase]
    probAll.sort(key=lambda x: x[1], reverse=True)
    probTop = probAll[:count]
    probTop.append((
        'others',
        round(1 - sum([e[1] for e in probTop]), precision)))

    return probTop
