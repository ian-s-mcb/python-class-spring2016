import tkinter as tk
import tkinter.font as tkFont
import tkinter.ttk as ttk


class Table(ttk.LabelFrame):
    """A table widget for displaying results from analyzer

    The table columns are: rank, word, freq, and stopword. Clicking on one of
    table's column names will sort the table's items by that column.

    This class extends ttk's Frame, and it contains one Treeview widget and one
    Scrollbar widget.

    This class is based off an example from the following page:
    https://www.daniweb.com/programming/software-development/threads/350266/creating-table-in-python
    """
    def __init__(self, parent):
        """Main constructor
        """
        ttk.Frame.__init__(self, parent, padding=(0, 5, 0, 0))

        # sets properties
        self.header = ['rank', 'word', 'freq', 'stopword']

        self.createTreeview()

        # applies layout
        self.grid(sticky='nsew')
        self.grid_columnconfigure(0, weight=1)
        self.grid_rowconfigure(0, weight=1)


    def createTreeview(self):
        """Creates empty Treeview and vertical Scrollbar
        """
        self.tree = ttk.Treeview(self, columns=self.header, show="headings")
        vsb = ttk.Scrollbar(self, orient="vertical", command=self.tree.yview)
        self.tree.configure(yscrollcommand=vsb.set)

        # creates tree heading
        for col in self.header:
            self.tree.heading(col,
                text=col.title(),
                command=lambda col=col: self.sortBy(col, 0))

            # adjusts the column's width to the header string
            self.tree.column(col, width=tkFont.Font().measure(col.title()))

        # applies layout
        self.tree.grid(row=0, column=0, sticky='nsew')
        vsb.grid(row=0, column=1, sticky='ns')


    def sortBy(self, col, descending):
        """Sorts Treeview's items by the given column

        col -- String, column name
        descending -- Int, 0 or 1, direction by which the sorting takes place
        """
        # grabs values to sort
        numeric = col == 'rank' or col == 'freq'
        data = [(int(self.tree.set(child, col)), child) if numeric
                else (self.tree.set(child, col), child)
                for child in self.tree.get_children('')]

        # sorts the data
        data.sort(reverse=descending)
        for i, item in enumerate(data):
            self.tree.move(item[1], '', i)

        # switches direction of next sort
        self.tree.heading(col,
            command=lambda col=col: self.sortBy(col, int(not descending)))


    def fillTable(self, data):
        """Fills table with given top words data

        data -- List, each element is a tuple containing (rank, word, freq, and
            stopword)
        """
        # deletes existing items
        self.tree.delete(*self.tree.get_children())

        for item in data:
            self.tree.insert('', 'end', values=item)

            # adjusts column's width if necessary to fit each value
            for i, val in enumerate(item):
                col_w = tkFont.Font().measure(val)
                if self.tree.column(self.header[i],width=None) < col_w:
                    self.tree.column(self.header[i], width=col_w)
