import tkinter as tk
import tkinter.ttk as ttk

import controls
import table


class MainApplication(ttk.Frame):
    """Main widget for application

    Contains controls and table widgets
    """
    def __init__(self, parent):
        """Main constructor
        """
        ttk.Frame.__init__(self, parent, padding=5)

        # widgets
        self.controls = controls.Controls(self)
        self.table = table.Table(self)

        # layouts
        self.grid_columnconfigure(0, weight=1)
        self.grid_rowconfigure(1, weight=1)
        self.grid(sticky='nsew')
        self.controls.grid(row=0, column=0)
        self.table.grid(row=1, column=0)


if __name__ == "__main__":
    root = tk.Tk()
    root.grid_columnconfigure(0, weight=1)
    root.grid_rowconfigure(0, weight=1)
    root.title('Top Words')
    app = MainApplication(root)
    app.mainloop()
