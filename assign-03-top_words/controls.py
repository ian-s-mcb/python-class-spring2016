import os
import tkinter as tk
from tkinter import filedialog
import tkinter.ttk as ttk

import analyzer
import table

class Controls(ttk.LabelFrame):
    """Allows the user to control the top word anaysis

    This class extends ttk's LabelFrame. Besides having Labels, this widget has
    an Entry for `count` (the numbers of words shown in the analysis results),
    a button to choose a file, and a button perform the analysis.
    """

    def __init__(self, parent):
        """Main constructor
        """
        ttk.LabelFrame.__init__(self, parent, text='Controls', padding=5)

        # properties
        self.parent = parent
        self.filenameDisplay = tk.StringVar(master=self, value='None')
        self.count = tk.StringVar(master=self, value='100')
        self.inputPath = None

        # widgets
        label1 = ttk.Label(self, text='File: ')
        label2 = ttk.Label(self, textvariable=self.filenameDisplay)
        label3 = ttk.Label(self, text='Count: ')
        entry1 = ttk.Entry(self, textvariable=self.count, width=3)
        button1 = ttk.Button(self, text='Choose...', command=self.chooseFile)
        button2 = ttk.Button(self, text='Analyze', command=self.analyzeFile)

        # layouts
        self.grid(sticky='new')
        widgets = [label1, label2, label3, entry1, button1, button2]
        for i, w in enumerate(widgets):
            w.grid(row=0, column=i, sticky='ew')
            self.grid_columnconfigure(i, weight=1)


    def chooseFile(self):
        """Let's the user choose a file to analyze via a file dialog
        """
        inputPath = self.inputPath = filedialog.askopenfilename(
            parent=self,
            filetypes=[('Text file', '.txt')])
        if inputPath:
            name = os.path.basename(inputPath)
            if (len(name) > 15):
                name = name[:15] + '...'
            self.filenameDisplay.set(name)


    def analyzeFile(self):
        """Analyzes a file and displays results

        Performs top word analysis on the set file. The count Entry specifies
        how many top words are calculated. If the Entry is blank or zero, all
        top words are calculated, otherwise `count` words are calculated. If
        the Entry is invalid, then 100 top words are calculated.

        After performing the analsis, the results are displayed in the table
        widget.
        """
        count = self.count.get()
        try:
            if count == '' or count == '0':
                count = None
            else:
                count = int(self.count.get())
                if count < 0:
                    raise
        except:
            self.count.set('100')
            count = 100

        data = analyzer.topWords(self.inputPath, count)
        self.parent.table.fillTable(data)
