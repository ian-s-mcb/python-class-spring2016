from collections import Counter
from prettytable import PrettyTable
import re
from nltk.corpus import stopwords
import string


def topWords(filePath, count):
    """Calculates top frequent words in a given text (non-regex version)

    Top words include stopwords. Each top word is indicated whether it is a
    stop word or not.

    filePath -- String, path to file that will be analyzed
    count -- Int, number of top words to calculate
    """
    counter = Counter()

    # all non alphabetic characters are to substituted with spaces
    translateTable = {}
    for p in (string.punctuation + string.digits):
        translateTable[ord(p)] = ' '
    stop_words = set(stopwords.words('english'))

    fileIn = open(filePath, 'r')
    for line in fileIn:
        # perform substitution and split on whitespace
        words = line.lower().translate(translateTable).split()
        counter.update(words)
    fileIn.close()

    most_common = counter.most_common(count)

    # identifies which words are stopwords
    # also adds word rank to list
    return [
        (i + 1, *t, True)
        if t[0] in stop_words
        else (i + 1, *t, False)
        for i,t in enumerate(most_common)]


def topWordsRegex(filePath, count):
    """Calculates top frequent words in a given text (regex version)

    Top words include stopwords. Each top word is indicated whether it is a
    stop word or not.

    filePath -- String, path to file that will be analyzed
    count -- Int, number of top words to calculate
    """
    counter = Counter()
    expression = r'[a-z]+'
    stop_words = set(stopwords.words('english'))

    fileIn = open(filePath, 'r')
    for line in fileIn:
        # finds all alphabetic tokens
        words = re.findall(expression, line.lower())
        counter.update(words)
    fileIn.close()

    most_common = counter.most_common(count)

    # identifies which words are stopwords
    # also adds word rank to list
    return [
        (i + 1, *t, True)
        if t[0] in stop_words
        else (i + 1, *t, False)
        for i,t in enumerate(most_common)]


def compareTopWords():
    """Compares the results from the two top words functions

    The two word ranking implementations (regular/non-regex and regex) are
    performed on emma.txt. All word frequencies are calculated, not just the
    top 100 ones.

    Running this function showed that the two implementations produce identical
    results.
    """
    outputRegular = topWords('./emma.txt', None)
    outputRegex = topWordsRegex('./emma.txt', None)
    condition = outputRegular == outputRegex
    print("A comparison of topWords and topWordsRegex shows that:")
    print("they are", "identical." if condition else "not identical.")


def printTable():
    """Prints a table of top words in emmma.txt

    Word ranking shows the top 100 most frequent words in emma.txt. Stopwords
    are included the ranking. Each word is indicated whether it is a stop word
    or not.
    """
    table = PrettyTable()
    table.field_names = ['Rank', 'Word', 'Freq', 'Stopword']
    data = topWords('./emma.txt', 100)
    for row in data:
        table.add_row(row)
    print(table)


if __name__ == '__main__':
    compareTopWords()
    printTable()
